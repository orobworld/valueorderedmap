package demo;

import java.util.Map;
import java.util.stream.Stream;
import valueorderedmap.ValueOrderedMap;

/**
 * This class demonstrates basic features of the ValueOrderedMap class.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Demo {

  /** Null constructor. **/
  private Demo() { }

  /**
   * Runs the demonstration.
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Create a mapping of strings to integers, ordered by the
    // integer values.
    ValueOrderedMap<String, Integer> map = new ValueOrderedMap<>();
    map.put("a", 1);
    map.put("b", 2);
    map.put("c", 3);
    map.put("d", 4);
    map.put("e", 5);
    // Display the mapping in both ascending and descending order.
    System.out.println("Original entries (ascending):");
    printStream(map.ascendingEntryStream());
    System.out.println("\nOriginal entries (descending):");
    printStream(map.descendingEntryStream());
    // Alter two entries, remove one and create a new one.
    map.put("b", 14);
    map.put("d", 0);
    map.remove("c");
    map.put("x", 1);
    // Reprint the stream (ascending and descending) to verify the changes.
    System.out.println("\nModified entries (ascending):");
    printStream(map.ascendingEntryStream());
    System.out.println("\nModified entries (descending):");
    printStream(map.descendingEntryStream());
    // Test for the presence of specific keys.
    System.out.println("\nContains key 'b'? " + map.containsKey("b"));
    System.out.println("Contains key 'h'? " + map.containsKey("h"));
    // Test for the presence of specific values.
    System.out.println("\nContains value 99? " + map.containsValue(99));
    System.out.println("Contains value 1? " + map.containsValue(1));
    // Show all the keys.
    System.out.println("\nKey set:\n" + map.keySet().toString());
    // Show all the values.
    System.out.println("\nValues:\n" + map.values().toString());
    // Show the keys in descending value order.
    System.out.println("\nKeys in descending value order:");
    map.descendingKeyStream().forEach((k) -> System.out.println(k));
    System.out.println("\nKeys in ascending value order:");
    map.ascendingKeyStream().forEach((k) -> System.out.println(k));
  }

  /**
   * Prints a stream of map entries.
   * @param s the stream to print
   */
  private static void printStream(final Stream<Map.Entry<String, Integer>> s) {
    s.forEach((e) -> System.out.println(e.getKey() + "->" + e.getValue()));
  }
}
