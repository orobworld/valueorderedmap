package valueorderedmap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ValueOrderedMap emulates an ordered map in which the ordering is based on
 * the values, rather than the keys, of entries.
 *
 * Ties are broken using the ordering of the keys.
 *
 * Neither keys nor values may be null.
 *
 * The map contents is made available as a stream of Java Map.Entry objects.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 * @param <K> the key class (must be comparable)
 * @param <V> the value class (must be comparable)
 */
public final class ValueOrderedMap<K extends Comparable<K>,
                                   V extends Comparable<V>> {

  private final TreeSet<Entry<K, V>> entries;
  private final HashMap<K, Entry<K, V>> keyMap;

  /**
   * Constructor for a new, initially empty map.
   */
  public ValueOrderedMap() {
    entries = new TreeSet<>();
    keyMap = new HashMap<>();
  }

  /**
   * Copy constructor.
   * @param map a map to copy
   */
  public ValueOrderedMap(final ValueOrderedMap<K, V> map) {
    entries = new TreeSet<>();
    keyMap = new HashMap<>();
    map.ascendingEntryStream()
       .forEach((e) -> this.put(e.getKey(), e.getValue()));
  }

  /**
   * Streams the entries in ascending value order.
   * @return a stream of map entries in ascending value order
   */
  public Stream<Map.Entry<K, V>> ascendingEntryStream() {
    return entries.stream().map((x) -> x);
  }

  /**
   * Streams the keys in ascending value order.
   * @return a stream of keys, in ascending value order
   */
  public Stream<K> ascendingKeyStream() {
    return entries.stream().map((e) -> e.getKey());
  }

  /**
   * Clears the map.
   */
  public synchronized void clear() {
    entries.clear();
    keyMap.clear();
  }

  /**
   * Tests for the presence of a specified key.
   * @param k the key
   * @return true if there is an entry with this key
   */
  public boolean containsKey(final K k) {
    return keyMap.containsKey(k);
  }

  /**
   * Tests for the presence of a particular value.
   * Testing is done using the .equals() method of the value class.
   * @param v the value being sought
   * @return true if at least one entry has that value
   */
  public boolean containsValue(final V v) {
    return entries.stream().anyMatch((e) -> e.getValue().equals(v));
  }

  /**
   * Streams the entries in descending value order.
   * @return a stream of map entries in descending value order
   */
  public Stream<Map.Entry<K, V>> descendingEntryStream() {
    return entries.descendingSet().stream().map((x) -> x);
  }

  /**
   * Streams the keys in descending value order.
   * @return a stream of keys, in descending value order
   */
  public Stream<K> descendingKeyStream() {
    return entries.descendingSet().stream().map((e) -> e.getKey());
  }

  /**
   * Gets an unordered set view of the map contents.
   * @return the set of map entries
   */
  public Set<Map.Entry<K, V>> entrySet() {
    return entries.stream().map((e) -> e).collect(Collectors.toSet());
  }

  /**
   * Gets the value of an entry.
   * @param k the entry's key
   * @return the entry's value (null if the key is not present)
   */
  public V get(final K k) {
    if (keyMap.containsKey(k)) {
      return keyMap.get(k).getValue();
    } else {
      return null;
    }
  }

  /**
   * Tests whether the mapping is empty.
   * @return true if empty
   */
  public boolean isEmpty() {
    return keyMap.isEmpty();
  }

  /**
   * Gets the set of keys.
   * @return the set of keys
   */
  public Set<K> keySet() {
    return keyMap.keySet();
  }

  /**
   * Adds a key-value pair to the map.
   * @param k the key for the new entry (must not be null)
   * @param v the value for the new entry (must not be null)
   */
  public synchronized void put(final K k, final V v) {
    // If the key is already present, remove the old entry.
    if (keyMap.containsKey(k)) {
      entries.remove(keyMap.get(k));
    }
    if (v == null) {
      // Assume that a null value means the user is trying to remove the key.
      keyMap.remove(k);
    } else {
      // Add the new entry.
      Entry<K, V> entry = new Entry<>(k, v);
      entries.add(entry);
      keyMap.put(k, entry);
    }
  }

  /**
   * Removes an entry (if present) with a given key.
   * @param k the key to remove
   */
  public synchronized void remove(final K k) {
    if (keyMap.containsKey(k)) {
      entries.remove(keyMap.get(k));
      keyMap.remove(k);
    }
  }

  /**
   * Gets the number of entries.
   * @return the number of entries
   */
  public int size() {
    return keyMap.size();
  }

  /**
   * Gets the collection of values present in the map.
   * @return the value set
   */
  public Collection<V> values() {
    return entries.stream()
                  .map((e) -> e.getValue())
                  .collect(Collectors.toSet());
  }

  /**
   * Entry is a container for a key value pair.
   * It implements the Java Map.Entry interface.
   * @param <K> the key type
   * @param <V> the value type
   */
  private class Entry<K extends Comparable<K>, V extends Comparable<V>>
                implements Comparable<Entry<K, V>>, Map.Entry<K, V> {
    private final K key;
    private final V value;

    /**
     * Constructor.
     * @param k the entry's key (must not be null)
     * @param v the entry's value (must not be null)
     */
    Entry(final K k, final V v) {
      // Do not allow null arguments.
      if (k == null) {
        throw new IllegalArgumentException("Null map keys not allowed.");
      } else if (v == null) {
        throw new IllegalArgumentException("Null map values not allowed.");
      }
      key = k;
      value = v;
    }

    /**
     * Compares this entry to another entry.
     * The primary comparison is by values. If the values are equal, ties
     * are broken by comparing keys.
     * @param o the other entry
     * @return -1 if this entry is first in sort order, 0 if they are tied,
     * +1 if the other entry is first
     */
    @Override
    public int compareTo(final Entry<K, V> o) {
      int d = value.compareTo(o.getValue());
      if (d == 0) {
        d = key.compareTo(o.getKey());
      }
      return d;
    }

    /**
     * Gets the entry's key.
     * @return the key
     */
    @Override
    public K getKey() {
      return key;
    }

    /**
     * Gets the entry's value.
     * @return the value
     */
    @Override
    public V getValue() {
      return value;
    }

    /**
     * Changes the value of an entry and pushes the change to the underlying
     * map.
     * This method is not supported!
     * @param v the new value
     * @return the old value
     */
    @Override
    public V setValue(final V v) {
      throw new UnsupportedOperationException("Not supported.");
    }
  }
}
