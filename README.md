#ValueOrderedMap

The **ValueOrderedMap** class file implements a collection that operates similarly to the Java `TreeMap` class, with one critical difference. Whereas `TreeMap` sorts its entries according to their keys, `ValueOrderedMap` sorts its entries according to their values.

A small demonstration program is provided in the `demo` package. `ValueOrderedMap` implements many but not all of the methods found in Java's `AbstractMap` class, plus a few additional methods (such as the stream interfaces). Methods that alter the map are synchronized, so the class *should* (maybe, hypothetically) be thread-safe.

Note that the only safe way to alter the contents of a `ValueOrderedMap` is via one of the methods designed for modifying it: `clear()`; `put()`; or `remove()`. Altering elements of a stream or keys (values) returned by `keySet()` (`values()`) will have unpredictable and quite possibly disastrous results.